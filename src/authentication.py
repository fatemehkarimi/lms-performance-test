'''
This class is used to authenticate to lms-learning management website.
@param login_url: the url on which login form exists
@param username: username used to login
@param password: password used to login
@function login: returns the SCG token. this token can be used for later requests
                 to website.
'''

from urllib import request, parse
from urllib.parse import urlparse
from bs4 import BeautifulSoup
from http.cookies import SimpleCookie

class Authentication:
    def __init__(self, login_url, username, password):
        self.login_url = login_url
        self.username = username
        self.password = password

    def parse_login_form(self):
        response = request.urlopen(self.login_url)
        html_response = response.read().decode('utf-8')
        bs_html = BeautifulSoup(html_response, features="html.parser")
        login_form = bs_html.body.find('form', attrs={'id': 'loginForm'})
        path = login_form['action']
        _eventId = login_form.find('input', attrs={'name': '_eventId'})['value']
        execution = login_form.find('input', attrs={'name': 'execution'})['value']
        
        return {
            'path': path,
            '_eventId': _eventId,
            'execution': execution
        }

    def login(self):
        form_info = self.parse_login_form()
        url_parser = urlparse(self.login_url)
        auth_path = url_parser.scheme + '://' +  url_parser.netloc + form_info['path']
        auth_info = {
            'username': self.username,
            'password': self.password,
            'execution': form_info['execution'],
            '_eventId': form_info['_eventId']
        }

        encoded_info = parse.urlencode(auth_info).encode("utf-8")
        req = request.Request(auth_path, encoded_info)
        response = request.urlopen(req)
        raw_cookie = response.info()['Set-Cookie']
        s_cookie = SimpleCookie()
        s_cookie.load(raw_cookie)
        
        cookies = {}
        for key, morsel in s_cookie.items():
            cookies[key] = morsel.value
        return cookies
