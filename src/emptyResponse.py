class EmptyResponse:
    def __init__(self, return_code):
        self.return_code = return_code
    
    def getcode(self):
        return self.return_code
