from stable_baselines.sac.policies import MlpPolicy
from stable_baselines import SAC

from responseTimeEnv import ResponseTimeEnv
from customSACPolicy import CustomSACPolicy

class Agent:
    def __init__(self, env, ttl, max_neg_reward,
                replay_memory_size=50000,layers=None):
        if layers:
            CustomSACPolicy.change_layer_structure(layers)

        self.env = env
        self.model = SAC(CustomSACPolicy, env,
                            buffer_size=replay_memory_size,
                            verbose=1,
                            tensorboard_log='./diagrams/')
        self.ttl = ttl
        self.max_neg_reward = max_neg_reward
        self.bottle_necks = []
        
    def find_bottle_necks(self):
        step = 0
        self.model.learn(1000, tb_log_name='result',
                        reset_num_timesteps=False)

        while step < self.ttl:
            obs = self.env.reset()
            done = False
            current_episode_reward = 0

            while step < self.ttl and not done:
                action, _ = self.model.predict(obs)
                new_obs, reward, done, info = self.env.step(action)
                current_episode_reward += reward

                if current_episode_reward <= self.max_neg_reward:
                    done = True

                print("current step = ", step)

                if reward > 0:
                    self.bottle_necks.append(obs[0])

                obs = new_obs
                step += 1
        return self.bottle_necks