from stable_baselines.sac.policies import FeedForwardPolicy

class CustomSACPolicy(FeedForwardPolicy):
    # This is the default value, you can change it.
    LAYERS = [32, 32, 32]
    def __init__(self, *args, **kwargs):
        super(CustomSACPolicy, self).__init__(*args, **kwargs,
                                            layers=CustomSACPolicy.LAYERS,
                                            layer_norm=True,
                                            feature_extraction="mlp")
    
    @classmethod
    def change_layer_structure(cls, layers):
        cls.LAYERS = layers
