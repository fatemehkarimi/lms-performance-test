from urllib import request, parse
from authentication import Authentication
from urllib.error import HTTPError

from emptyResponse import EmptyResponse
class Connection:
    def __init__(self, target):
        self.auth_required = False
        self.target = target

    def __init__(self, target, login_url, username, password):
        self.target = target
        self.auth_required = True
        authentication = Authentication(login_url, username, password)
        self.auth_cookie = authentication.login()

    def __init__(self, target, auth_cookie):
        self.target = target
        self.auth_required = True
        self.auth_cookie = auth_cookie

    def get_url(self, path):
        req = request.Request(self.target + path)
        if self.auth_required:
            req.add_header('Cookie', self.auth_cookie)
        
        try:
            response = request.urlopen(req)
        except HTTPError as err:
            return EmptyResponse(err.code)
        else: 
            return response
