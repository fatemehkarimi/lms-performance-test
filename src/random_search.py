import time
import random
import numpy as np
import matplotlib.pyplot as plt
from config import TEST_COURSE_VIEW, LMS_URL
from connection import Connection
from responseTimeEnv import ResponseTimeEnv

def plot_diagram(output_file, arr):
    plt.plot(arr, len(arr) * [1] ,"x")
    plt.savefig(output_file)

def main():
    env = ResponseTimeEnv(LMS_URL,
                        'MoodleSession=10a9gs4itnso65bgi72qfogkk5',
                        TEST_COURSE_VIEW)
    ttl = TEST_COURSE_VIEW['ttl']

    b_necks = []
    env.reset()
    for i in range(ttl):
        action = random.uniform(-TEST_COURSE_VIEW['action-space'],
                                TEST_COURSE_VIEW['action-space'])

        new_obs, reward, _, _ = env.step(np.array([action]))
        if reward > 0:
            b_necks.append(new_obs[0])

    print(b_necks)
    plot_diagram("output-" + str(int(time.time())), b_necks)
    



if __name__ == '__main__':
    main()
