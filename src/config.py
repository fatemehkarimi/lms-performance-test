DEBUG = True
LMS_URL = 'https://yekta.iut.ac.ir'

TEST_COURSE_VIEW = {
    # Environment configuration
    'path': '/course/view.php?id={}',
    'left-interval': 10000,
    'right-interval': 20000,
    'threshold': 2,
    'reward': 5,
    'punishment': -1,
    'action-space': 0.05,

    # Agent configuration
    'replay-memory-size': 3000,
    'layer-structure': [32, 32],
    'ttl': 2500,
    'max-negative-reward': -20,
}