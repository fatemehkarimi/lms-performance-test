import matplotlib.pyplot as plt
import time
from config import LMS_URL, TEST_COURSE_VIEW
from responseTimeEnv import ResponseTimeEnv
from agent import Agent

def check_test_config_is_valid(test_config):
    assert TEST_COURSE_VIEW['punishment'] >= TEST_COURSE_VIEW['max-negative-reward'],\
         "Test case config is not standard"

    assert TEST_COURSE_VIEW['action-space'] > 0, \
         "max value for action cannot be negative"

def plot_diagram(output_file, arr):
    plt.plot(arr, len(arr) * [1] ,"x")
    plt.savefig(output_file)

def main():
    env = ResponseTimeEnv(LMS_URL,
	                    'MoodleSession=1fj47bk79sus9j8q1o2vs9rqsk',
                        TEST_COURSE_VIEW)
    
    agent = Agent(env, TEST_COURSE_VIEW['ttl'],
                TEST_COURSE_VIEW['max-negative-reward'],
                replay_memory_size=TEST_COURSE_VIEW['replay-memory-size'],
                layers=TEST_COURSE_VIEW['layer-structure'])

    b_necks = agent.find_bottle_necks()
    print(b_necks)
    plot_diagram("output-" + str(int(time.time())), b_necks)


if __name__ == '__main__':
    main()
