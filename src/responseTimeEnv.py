import time
import random
import gym
import numpy as np
from connection import Connection
from config import DEBUG


class ResponseTimeEnv(gym.Env):
    
    def __init__(self, target, auth_token, test_config):
        super().__init__()

        self.connection = Connection(target, auth_token)
        self.path = test_config['path']
        self.MIN_STATE_VALUE = test_config['left-interval']
        self.MAX_STATE_VALUE = test_config['right-interval']
        self.threshold = test_config['threshold']
        self.reward = test_config['reward']
        self.punishment = test_config['punishment']
        self.create_history()

        self.action_space = gym.spaces.Box(low=-test_config['action-space'],
                                            high=test_config['action-space'],
                                            shape=(1,), dtype=np.float32)


        self.observation_space = gym.spaces.Box(
            low=np.array([self.MIN_STATE_VALUE]),
            high=np.array([self.MAX_STATE_VALUE]),
            dtype=np.int64
        )

    def create_history(self):
        self.history = set()

    def get_response_time(self, path, args):
        path = path.format(*args)

        start_time = time.time()
        response = self.connection.get_url(path)

        end_time = time.time()

        if DEBUG:
            self.report(path, response, end_time - start_time)

        return end_time - start_time

    def get_reward(self, state, elapsed_time):
        if elapsed_time > self.threshold and not state in self.history:
            return self.reward
        else:
            return self.punishment

    def step(self, action):
        range_size = self.MAX_STATE_VALUE - self.MIN_STATE_VALUE
        new_state = self.state[0] + int(range_size * action[0])
        new_state = np.array([new_state])
        new_state = np.clip(new_state,
                            self.MIN_STATE_VALUE, self.MAX_STATE_VALUE)

        
        response_time = self.get_response_time(self.path, new_state)

        reward = self.get_reward(new_state[0], response_time)
        self.history.add(new_state[0])
        self.state = new_state

        return new_state, reward, False, {}

    def reset(self):
        random_state = random.randint(self.MIN_STATE_VALUE, self.MAX_STATE_VALUE)
        self.state = np.array([random_state], dtype=np.int64)
        return self.state

    def render(self):
        pass

    def close(self):
        pass


    def report(self, path, response, time_elapsed):
        print("REQUEST URL = ", path, 'RETURN CODE = ', 
                response.getcode(), 'TIME ELAPSED = ', time_elapsed)

